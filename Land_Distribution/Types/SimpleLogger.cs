﻿using System.Collections.Generic;
using System.IO;
using Logging;

namespace Land_Distribution.Types
{
    public class SimpleLogger: Logger
    {
        private const string FILEPATH = "log.txt";
        
        public SimpleLogger()
        {
            AddSource += AddToTextFile;
        }

        private void AddToTextFile(string message)
        {
            using (StreamWriter sw = new StreamWriter(FILEPATH, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(message);
            }
        }

        public List<string> GetLogsList()
        {
            List<string> result = new List<string>();
                
            using (StreamReader sr = new StreamReader(FILEPATH, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    result.Add(line);
                }
            }

            return result;
        }
    }
}