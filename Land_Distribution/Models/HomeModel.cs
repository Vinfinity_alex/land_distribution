﻿using System.Collections.Generic;
using Land_Distribution.Types;

namespace Land_Distribution.Models
{
    public class HomeModel
    {
        public List<Vector2> Points { get; set; }
    }
}