﻿using System;
using System.Web.Mvc;
using Autofac;
using Land_Distribution.MathSource;
using Land_Distribution.Models;
using Land_Distribution.Types;
using Logging;

namespace Land_Distribution.Controllers
{
    public class HomeController : Controller
    {
        public HomeModel Model;

        private SimpleLogger _logger;

        public HomeController()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<SimpleLogger>();
            var container = builder.Build();
            _logger = container.Resolve<SimpleLogger>();
            
            _logger.Info("Main page was opened");
            
            Model = new HomeModel();
        }
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Log()
        {
            ViewBag.Message = "Your application description page.";

            string result = "";
            _logger.GetLogsList().ForEach(item => result += $"<li>{item}</li>");
            
            return Content(result);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        
        [HttpPost]
        public ActionResult SavePoints(string function_param)
        {
            _logger.Info("Send a request for area calculation");
            try
            {
                Model.Points = Vector2.FromJson(function_param);
            }
            catch (Exception ex)
            {
                _logger.Error("Data parse error");
            }

            float area;
            string context;
            string result;
            
            if (AreaCalculator.Compute(Model.Points, out area))
            {
                string str = $"Area: {area} sq. m";
                _logger.Info(str);
                context = str;
            }
            else
            {
                string str = "The area has been calculated incorrectly or the parameters are incorrect";
                _logger.Warn(str);
                context = str;
            }
            
            result = $"<h3>{context}</h3>";
            
            return Content(result);
        }

        [HttpPost]
        public void AddNewPoints()
        {
            _logger.Info("New point added");
        }
    }
}