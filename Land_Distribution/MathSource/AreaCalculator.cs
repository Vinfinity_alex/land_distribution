﻿using System.Collections.Generic;
using System.Linq;
using Land_Distribution.Types;

namespace Land_Distribution.MathSource
{
    public class AreaCalculator
    {
        public static bool Compute(List<Vector2> pointsList, out float area)
        {
            float halfSumY = pointsList.Select(item => item.X).Sum() / 2.0f;
            float halfSumX = pointsList.Select(item => item.Y).Sum() / 2.0f;
            bool result = halfSumY == halfSumX;

            if (result) area = halfSumY;
            else area = 0;
            
            return result;
        }
    }
}