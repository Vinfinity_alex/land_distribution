﻿using Autofac;
//using Autofac.Integration.Mvc;
using System.Web.Mvc;
using Autofac.Integration.Mvc;
using Land_Distribution.Types;
using Logging;

namespace Land_Distribution.Utils
{
 
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();
 
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
 
            builder.RegisterType<SimpleLogger>().As<ILogger>();
         
            var container = builder.Build();
 
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}