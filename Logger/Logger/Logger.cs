﻿using System;
using System.Collections.Generic;

namespace Logging
{
    public interface ILogger
    {
        void Info(string message);
        void Warn(string message);
        void Error(string message);
        void Fatal(string message);
    }

    public abstract class Logger : ILogger
    {
        public event Action<string> AddSource;

        public List<LogData> LogList { get; private set; }

        public Logger()
        {
            LogList = new List<LogData>();
        }

        public virtual void Error(string message)
        {
            AddToStorage(AddDateTime(message));
            AddToBuffer(LogType.Error, message);
        }

        public virtual void Fatal(string message)
        {
            AddToStorage(AddDateTime(message));
            AddToBuffer(LogType.Fatal, message);
        }

        public virtual void Info(string message)
        {
            AddToStorage(AddDateTime(message));
            AddToBuffer(LogType.Info, message);
        }

        public virtual void Warn(string message)
        {
            AddToStorage(AddDateTime(message));
            AddToBuffer(LogType.Warn, message);
        }

        public void AddToStorage(string message)
        {
            AddSource?.Invoke(message);
        }

        public void AddToBuffer(LogType logType, string message)
        {
            LogList.Add(new LogData { LogType = logType, Message = message });
        }

        private string AddDateTime(string message)
        {
            return message + " : " + DateTime.Now;
        }
    }

    public struct LogData
    {
        public LogType LogType;
        public string Message;
    }

    public enum LogType { Error, Warn, Info, Fatal }
}
